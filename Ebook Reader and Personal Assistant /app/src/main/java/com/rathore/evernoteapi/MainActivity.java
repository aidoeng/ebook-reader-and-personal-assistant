package com.rathore.evernoteapi;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.evernote.auth.EvernoteAuth;
import com.evernote.auth.EvernoteService;
import com.evernote.client.android.EvernoteOAuthActivity;
import com.evernote.client.android.EvernoteSession;
import com.evernote.client.android.EvernoteUtil;
import com.evernote.client.android.asyncclient.EvernoteCallback;
import com.evernote.client.android.asyncclient.EvernoteNoteStoreClient;
import com.evernote.client.android.login.EvernoteLoginFragment;
import com.evernote.clients.ClientFactory;
import com.evernote.clients.NoteStoreClient;
import com.evernote.edam.error.EDAMSystemException;
import com.evernote.edam.error.EDAMUserException;
import com.evernote.edam.type.Note;
import com.evernote.edam.type.Notebook;
import com.evernote.thrift.TException;

import java.util.ArrayList;
import java.util.List;


public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener,EvernoteLoginFragment.ResultCallback {


    private static final String CONSUMER_KEY ="vinita-8358";
    private static final String CONSUMER_SECRET = "1d8f85324fd57165";




    private static final EvernoteSession.EvernoteService EVERNOTE_SERVICE = EvernoteSession.EvernoteService.SANDBOX;

//1d8f85324fd57165
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


//
////        EvernoteAuth evernoteAuth = new EvernoteAuth(EvernoteService.SANDBOX, developerToken);
////        ClientFactory factory = new ClientFactory(evernoteAuth);
////        try {
////            NoteStoreClient noteStore = factory.createNoteStoreClient();
////
////        } catch (EDAMUserException e) {
////            e.printStackTrace();
////        } catch (EDAMSystemException e) {
////            e.printStackTrace();
////        } catch (TException e) {
////            e.printStackTrace();
////        }


        //Set up the Evernote singleton session, use EvernoteSession.getInstance() later
        new EvernoteSession.Builder(this)
                .setEvernoteService(EVERNOTE_SERVICE)
                .setSupportAppLinkedNotebooks(true)
                .setForceAuthenticationInThirdPartyApp(true)
//                .setLocale(Locale.SIMPLIFIED_CHINESE)
                .build(CONSUMER_KEY, CONSUMER_SECRET)
                .asSingleton();
        EvernoteSession.getInstance().authenticate(MainActivity.this);

//        registerActivityLifecycleCallbacks(new LoginChecker());




        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        this.setTitle("All Notes");

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


//        if (!EvernoteSession.getInstance().isLoggedIn()) {
//            return;
//        }

//        EvernoteNoteStoreClient noteStoreClient = EvernoteSession.getInstance().getEvernoteClientFactory().getNoteStoreClient();
//
//        Note note = new Note();
//        note.setTitle("My title");
//        note.setContent(EvernoteUtil.NOTE_PREFIX + "My content" + EvernoteUtil.NOTE_SUFFIX);
//
//        noteStoreClient.createNoteAsync(note, new EvernoteCallback<Note>() {
//            @Override
//            public void onSuccess(Note result) {
//                Toast.makeText(getApplicationContext(), result.getTitle() + " has been created", Toast.LENGTH_LONG).show();
//            }
//
//            @Override
//            public void onException(Exception exception) {
//                Log.i("Error creating note", String.valueOf(exception));
//            }
//        });

        if (!EvernoteSession.getInstance().isLoggedIn()) {
            return;
        }


//        noteStoreClient.listNotebooksAsync(new EvernoteCallback<List<Notebook>>() {
//            @Override
//            public void onSuccess(List<Notebook> result) {
//                List<String> namesList = new ArrayList<>(result.size());
//                for (Notebook notebook : result) {
//                    namesList.add(notebook.getName());
//                }
//                String notebookNames = TextUtils.join(", ", namesList);
//                Toast.makeText(getApplicationContext(), notebookNames + " notebooks have been retrieved", Toast.LENGTH_LONG).show();
//            }
//
//            @Override
//            public void onException(Exception exception) {
//                Log.i( "Errorretrievingnotbok", String.valueOf(exception));
//            }
//        });

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.allnotes) {
            // Handle the camera action
            Intent intent=new Intent(getApplicationContext(),NotesActivity.class);
            startActivity(intent);




        } else if (id == R.id.notebook) {

        } else if (id == R.id.sync) {}

//        } else if (id == R.id.nav_manage) {
//
//        } else if (id == R.id.nav_share) {
//
//        } else if (id == R.id.nav_send) {
//
//        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }



//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//
//        switch (requestCode) {
//            case EvernoteSession.REQUEST_CODE_LOGIN:
//                if (resultCode == Activity.RESULT_OK) {
//                    // handle success
//
//                } else {
//                    // handle failure
//                    Log.i("loggin","not logged in");
//                }
//                break;
//
//            default:
//                super.onActivityResult(requestCode, resultCode, data);
//                break;
//        }



    //}

    @Override
    public void onLoginFinished(boolean successful) {
        if(successful){
            Log.i("login","successful");
        }
        else {
            Log.i("login","unsuccessful");
        }
    }
}
