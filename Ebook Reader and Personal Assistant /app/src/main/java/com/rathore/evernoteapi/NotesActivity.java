package com.rathore.evernoteapi;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.webkit.MimeTypeMap;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.evernote.client.android.EvernoteSession;
import com.evernote.client.android.EvernoteUtil;
import com.evernote.client.android.asyncclient.EvernoteCallback;
import com.evernote.client.android.asyncclient.EvernoteNoteStoreClient;
import com.evernote.client.conn.mobile.FileData;
import com.evernote.edam.type.Note;
import com.evernote.edam.type.Notebook;
import com.evernote.edam.type.Resource;
import com.evernote.edam.type.ResourceAttributes;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class NotesActivity extends AppCompatActivity {
    EditText notesTitle,notesContent,notebook;
    String notetitle,noteContent,noteBook;
    EvernoteNoteStoreClient noteStoreClient;
    Resource resource;

    public void takeimage(View view){
//        Intent intent=new Intent();
      /// Intent intent=new Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        //intent.setType("image/*");
        //intent.setAction(Intent.ACTION_GET_CONTENT);
        //Intent.createChooser(intent,"selected file",1);
      // // startActivityForResult(Intent.createChooser(intent, "Select Picture"), 1);
        final CharSequence[] items = {"Camera","Select image","Select video","Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(this,R.style.MyDialogTheme);
        builder.setTitle("Add file");
        //builder.setIcon(R.drawable.ic_photo_black_24dp);
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Camera")) {
                    Intent i = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(i, 1);
                } else if (items[item].equals("Select image")) {
                    Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    i.setType("image/*");
                    startActivityForResult(Intent.createChooser(i, "Select image"), 2);
                } else if (items[item].equals("Select video")) {
                    Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                    intent.setType("video/*");
                    startActivityForResult(Intent.createChooser(intent, "Select video"), 3);
                }else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notes);
         noteStoreClient = EvernoteSession.getInstance().getEvernoteClientFactory().getNoteStoreClient();
//        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);




        notesTitle=(EditText)findViewById(R.id.noteTitle);
        notesContent=(EditText)findViewById(R.id.content);
        notebook=(EditText)findViewById(R.id.noteBookTitle);



        notesTitle.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    // do your stuff here
                    notetitle=notesTitle.getText().toString();
                    Log.i("noteTitle",notetitle);



                }
                return false;
            }
        });







    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        noteContent=notesContent.getText().toString();
        noteBook=notebook.getText().toString();

        Log.i("notecontent",noteContent+" , "+notetitle);
        if(notetitle!=null&&noteContent!=null) {
            //createNote(notetitle, noteContent);
        }
        else {
            Log.i("createNote","something is mising");
        }

        if(noteBook!=null){
        createNotebook(noteBook);
    }
    else {
            Log.i("createNotebook","something is mising");
        }
    }

    public void createNote(String title, String content){
    //EvernoteNoteStoreClient noteStoreClient = EvernoteSession.getInstance().getEvernoteClientFactory().getNoteStoreClient();

    Note note = new Note();

    //Log.i("title",title);
        Log.i("content",content);
        note.addToResources(resource);

    note.setTitle(title);

    note.setContent(EvernoteUtil.NOTE_PREFIX+content+EvernoteUtil.createEnMediaTag(resource)+EvernoteUtil.NOTE_SUFFIX);
    noteStoreClient.createNoteAsync(note, new EvernoteCallback<Note>() {
        @Override
        public void onSuccess(Note result) {
            Toast.makeText(getApplicationContext(), result.getTitle() + " has been created", Toast.LENGTH_LONG).show();
        }

        @Override
        public void onException(Exception exception) {
            Log.i("Error creating note", String.valueOf(exception));
        }
    });


}
public void createNotebook(String noteBookTitle){
    Notebook notebook=new Notebook();
    notebook.setName(noteBookTitle);
    noteStoreClient.createNotebookAsync(notebook, new EvernoteCallback<Notebook>() {
        @Override
        public void onSuccess(Notebook result) {
            Log.i("notebook","created");
        }

        @Override
        public void onException(Exception exception) {
            Log.i("notebook","not created");
        }
    });


    noteStoreClient.listNotebooksAsync(new EvernoteCallback<List<Notebook>>() {
        @Override
        public void onSuccess(List<Notebook> result) {
            List<String> namesList = new ArrayList<>(result.size());
            for (Notebook notebook : result) {
                namesList.add(notebook.getName());
            }
            String notebookNames = TextUtils.join(", ", namesList);
            Toast.makeText(getApplicationContext(), notebookNames + " notebooks have been retrieved", Toast.LENGTH_LONG).show();

        }

        @Override
        public void onException(Exception exception) {
            Log.i( "Errorretrievingnotbok", String.valueOf(exception));
        }
    });


}

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Uri selectedImage = null;
        if ( resultCode == RESULT_OK && data != null && data.getData() != null) {
    if(requestCode==1) {
     Log.i("imageeee","yess");
        selectedImage = data.getData();
        Bitmap photo = (Bitmap) data.getExtras().get("data");
        Uri tempUri = getImageUri( photo);
        String imagepath = String.valueOf(selectedImage);
    Log.i("imagepath", imagepath);

        Cursor cursor = getContentResolver().query(tempUri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        String path= cursor.getString(idx);
        File finalFile = new File(path);
        Log.i("final path",finalFile.getAbsolutePath());

//        String[] projection = {MediaStore.Images.Media.DATA};
//
//                Cursor cursor = getContentResolver().query(selectedImage, projection, null, null, null);
//                Log.i("collength", String.valueOf(cursor.getColumnNames().length));
//                cursor.moveToFirst();
//
//                Log.i("imagee", DatabaseUtils.dumpCursorToString(cursor));
//
//                int columnIndex = cursor.getColumnIndex(projection[0]);
//                String picturePath = cursor.getString(columnIndex);
//                String mimetype = getMimeType(picturePath);
//                // returns null
//                Log.i("picpath", picturePath);
//                Log.i("mimetype", mimetype);
//                cursor.close();

}           if(requestCode==2) {
                 selectedImage = data.getData();
                String imagepath = String.valueOf(selectedImage);
                Log.i("imagepath", imagepath);
                String[] projection = {MediaStore.Images.Media.DATA};
                Cursor cursor = getContentResolver().query(selectedImage, projection, null, null, null);
                Log.i("collength", String.valueOf(cursor.getColumnNames().length));
                cursor.moveToFirst();

                Log.i("imagee", DatabaseUtils.dumpCursorToString(cursor));

                int columnIndex = cursor.getColumnIndex(projection[0]);
                String picturePath = cursor.getString(columnIndex);
                String mimetype = getMimeType(picturePath);
                // returns null
                Log.i("picpath", picturePath);
                Log.i("mimetype", mimetype);
                cursor.close();
                setAttributes(picturePath,selectedImage);

            }
            if(requestCode==3) {
                 selectedImage = data.getData();
                String imagepath = String.valueOf(selectedImage);
                Log.i("vediopath", imagepath);
                Log.i("vediopath1", selectedImage.getPath());

                // MEDIA GALLERY
               String selectedImagePath = getPath(selectedImage);
                Log.i("vediopath2", selectedImagePath);
                setAttributes(selectedImagePath,selectedImage);
            }


        }
        else {
            Log.i("imageStatus","null");
        }
    }


    public static String getMimeType(String url) {
        String type = null;
        String extension = MimeTypeMap.getFileExtensionFromUrl(url);
        if (extension != null) {
            type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
        }
        return type;
    }
    public String getPath(Uri uri) {
//        String[] projection = { MediaStore.Video.Media.DATA };
//        Cursor cursor = getContentResolver().query(uri, projection, null, null, null);
//        if (cursor != null) {
//            // HERE YOU WILL GET A NULLPOINTER IF CURSOR IS NULL
//            // THIS CAN BE, IF YOU USED OI FILE MANAGER FOR PICKING THE MEDIA
//            int column_index = cursor
//                    .getColumnIndexOrThrow(MediaStore.Video.Media.DATA);
//            cursor.moveToFirst();
//            return cursor.getString(column_index);
//        } else
//            return null;

            String filePath = null;
            final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;
            if(isKitKat){
                filePath = generateFromKitkat(uri);
            }

            if(filePath != null){
                return filePath;
            }

            Cursor cursor = getContentResolver().query(uri, new String[] { MediaStore.MediaColumns.DATA }, null, null, null);

            if (cursor != null) {
                if (cursor.moveToFirst()) {
                    int columnIndex = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
                    filePath = cursor.getString(columnIndex);
                }
                cursor.close();
            }
        Log.i("filepath",filePath);
        return filePath == null ? uri.getPath() : filePath;
        }


    @TargetApi(19)
    private String generateFromKitkat(Uri uri){
        String filePath = null;
        if(DocumentsContract.isDocumentUri(this, uri)){
            String wholeID = DocumentsContract.getDocumentId(uri);

            String id = wholeID.split(":")[1];

            String[] column = { MediaStore.Video.Media.DATA };
            String sel = MediaStore.Video.Media._ID + "=?";


            Cursor cursor = getContentResolver().
                    query(MediaStore.Video.Media.EXTERNAL_CONTENT_URI,
                            column, sel, new String[]{ id }, null);



            int columnIndex = cursor.getColumnIndex(column[0]);

            if (cursor.moveToFirst()) {
                filePath = cursor.getString(columnIndex);
            }

            cursor.close();
        }
        return filePath;


    }
    public void setAttributes(String path,Uri uri){
        InputStream in = null;
        try {
            //Hash the data in the image file. The hash is used to reference the file in the ENML note content. 
            String mimetype = getMimeType(path);
            in = new BufferedInputStream(new FileInputStream(path));
            FileData data1=new FileData(EvernoteUtil.hash(in),new File(path));
            ResourceAttributes attributes=new ResourceAttributes();
            attributes.setFileName(uri.toString());

// Create a new Resource 
            resource=new Resource();
            resource.setData(data1);
            resource.setMime(mimetype);
            resource.setAttributes(attributes);
            createNote("xyz","asdfgh");
            //note.addToResources(resource);  
//                // Set the note's ENML content 
//                   String content = EvernoteUtil.NOTE_PREFIX  + EvernoteUtil.createEnMediaTag(resource) + EvernoteUtil.NOTE_SUFFIX;  
//                   note.setContent(content);      return createNote(note);  
        } catch(FileNotFoundException e){
            e.printStackTrace();
        }catch(IOException e){
            e.printStackTrace();
        }
        finally {
            if(in!=null){

                try{
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
    }
}
    public Uri getImageUri( Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }
}



